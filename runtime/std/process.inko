import std::conversion::ToInteger

let PRIMARY_POOL = 0
let IO_POOL = 1

## Returns the PID of the current process.
##
## # Examples
##
## Getting the PID of a process:
##
##     import std::process
##
##     process.current # => 0
def current -> Integer {
  _INKOC.process_current_pid
}

## Sends a message to a process, returning the message that was sent.
##
## # Examples
##
## Sending a message:
##
##     import std::process
##
##     let pid = process.spawn {
##       process.receive # => 'hello'
##     }
##
##     process.send(pid, 'hello') # => 'hello'
def send!(T)(pid: ToInteger, message: T) -> T {
  _INKOC.process_send_message(pid.to_integer, message)
}

## Receives a process message.
##
## Calling this method will block the current process until a message is
## received.
##
## The argument of this method can be used to set a timeout (in milliseconds).
## If no message is received and the timeout expires this method will return
## `Nil`.
##
## Messages are received in the same order in which they are sent.
##
## # Examples
##
## Receiving a message:
##
##     import std::process
##
##     process.send(process.current, 'hello')
##
##     process.receive # => 'hello'
##
## Receiving a message with a timeout:
##
##     import std::process
##
##     process.receive(5) # => Nil
def receive(timeout: ?Integer = Nil) {
  _INKOC.process_receive_message(timeout)
}

## Spawns a new process that will execute the given lambda.
##
## Processes are completely isolated and as such "self" in the lambda will refer
## to the module the lambda was created in.
##
## # Examples
##
## Spawning a process:
##
##     import std::process
##
##     process.spawn {
##       10 # => 10
##     }
def spawn(block: lambda) -> Integer {
  _INKOC.process_spawn(block, PRIMARY_POOL)
}

## Spawns a process that will perform IO (or other blocking) operations.
##
## IO processes are scheduled on a separate process pool so they don't intervene
## with regular processes.
##
## # Examples
##
## Spawning an IO process:
##
##     import std::process
##
##     process.spawn_io {
##       10 # => 10
##     }
##
def spawn_io(block: lambda) -> Integer {
  _INKOC.process_spawn(block, IO_POOL)
}

## Returns the status of a process as an `Integer`.
##
## The following values can be returned:
##
## * 0: The process has been scheduled.
## * 1: The process is running.
## * 2: The process has been suspended.
## * 3: The process has been suspended for garbage collection.
## * 4: The process is waiting for a message to arrive.
## * 5: The process finished execution.
##
## If a process does not exist (any more) then the status will also be `3`.
##
## # Examples
##
## Getting the status of a process:
##
##     import std::process
##
##     process.status(process.current) # => 1
def status(pid: ToInteger) -> Integer {
  _INKOC.process_status(pid.to_integer)
}

## Suspends the current process until it is rescheduled.
##
## The argument of this method can be used to set a minimum suspension time (in
## milliseconds). If no timeout is specified the process may be rescheduled at
## any time.
##
## # Examples
##
## Suspending a process:
##
##     import std::process
##
##     process.suspend # => Nil
##
## Suspending a process for a minimum amount of time:
##
##     import std::process
##
##     process.suspend(5) # => Nil
def suspend(timeout: ?Integer = Nil) -> Nil {
  _INKOC.process_suspend_current(timeout)
  Nil
}
